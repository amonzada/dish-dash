from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *

# iniciando janela
janela = Window(1200, 540) # tamanho redimensionado de um background // ver o tamanho certo que vai ficar a tela para redimensionar todos os backgrounds e acertar as posições do player (camadas e pulo) para esta fase
janela.set_title("Dish Dash")
tecla = Window.get_keyboard()

# iniciando fundos
# fundo = GameImage("assets/pv.jpg")
fundo1 = GameImage('assets/b1.jpeg')
fundo2 = GameImage('assets/b2.jpeg')
fundo1.x = 0
fundo1.y = 0

fundo2.x = fundo1.width
fundo2.y = 0

background_roll_speed = 150

def scrolling(bg1, bg2, roll_speed):
    # faz os fundos diminuirem o X simultaneamente
    bg1.x -= roll_speed * janela.delta_time()
    bg2.x -= roll_speed * janela.delta_time()
 
    # quando o background 2 (da direita) chegar com o X no início, volta como tava // isso é para aqueles fundos infinitos que se conectam, deixei pra testar
    if bg2.x <= 0:
        bg1.x = 0
        bg2.x = bg2.height
    
    # desenha o frame atualizado
    bg1.draw()
    bg2.draw()

# iniciando player
player = Sprite("assets/player.png")
player.set_position((janela.width - player.width)/2 - 200, janela.height/2 - 100)

# declaração de variáveis auxiliares
jump_count = 0
jump_vel = 500
gravidade = 1200

player_vel = 500 # velocidade do player

jumping = False # estado booleano dos movimentos básicos do player, para não executar ao mesmo tempo
down = False
up = False

layer = 1 # camada que o jogador se encontra (1: calçada, 0: rua)

#game loop
while True:
    # movimentação manual do jogador pros lados
    if tecla.key_pressed("d"):
        player.move_x(player_vel * janela.delta_time()) 
    if tecla.key_pressed("a"): # trocar essas duas condições para um move.x constante ou por nada, se for bastar o fundo se mexer sozinho
        player.move_x(-player_vel * janela.delta_time())

    # pressionar w ou s para trocar entre calçada ou rua (layer 0/1) - nâo é permitida a troca durante o movimento e ela só troca no meio do caminho (não quando o movimento se completa, nem quando o comando é dado) para evitar glitch - não foi implementado
    # tentei por longas 3 horas otimizar esse código, seja aglutinando ou fazendo def, o pplay é uma benção e só funciona quando quer!

    if not up and not down and not jumping: # verificação se pode fazer o movimento
        if tecla.key_pressed("w") and layer == 0:
            up = True # inicia o comando
            jump_count = jump_vel
    else:
        player.move_y(-jump_count * janela.delta_time())
        jump_count -= gravidade * janela.delta_time()
        if jump_count <= 222: # esse número define a posição máxima que o player vai ficar visualmente depois de subir, deve ser ajustado de acordo com tamanho da tela ou background
            layer = 1
            up = False # encerra o processo de subir, pois chegou no limite

    if not up and not down and not jumping:
        if tecla.key_pressed("s") and layer == 1:
            down = True
            jump_count = -jump_vel
    else:
        player.move_y(jump_count * janela.delta_time())
        jump_count += gravidade * janela.delta_time()
        if player.y >= 240:
            layer = 0
            down = False

    # mecânica de pular (mantém a layer, não se pula de uma pra outra)
    if not up and not down and not jumping:
        if tecla.key_pressed("space"):
            jumping = True
            jump_count = jump_vel
    else:
        player.move_y(-jump_count * janela.delta_time())
        jump_count -= gravidade * janela.delta_time() 
        if jump_count <= (-jump_vel):
            jumping = False
            
    # teste de colisão com os lados X - Tirar isso quando implementar a movimentação adequada
    if player.x >= janela.width - player.width:
        player.x = janela.width - player.width
    if player.x <= 0:
        player.x = 0 # buga pois o tamanho do boneco não é só a parte visível (ver PNG)

    #fundo.draw()
    scrolling(fundo1, fundo2, background_roll_speed)
    player.draw()
    janela.update()